#!/usr/bin/python
# -*- coding: utf-8 -*-
# RæMote for Maemo
# Copyright (c) 2010-03-08 Thomas Perl <thpinfo.com/about>
# Licensed under the terms of the GNU GPL v3 or later

import os
import subprocess
import gtk
import gobject
import cairo
import hildondesktop
import osso

def restart_lirc():
    subprocess.Popen(['sudo', '/etc/init.d/lirc', 'restart'])

def send_ir(remote, code):
    subprocess.Popen(['sudo', 'irsend', 'SEND_ONCE', remote, code])

class Rectangle(object):
    def __init__(self, action, x1, y1, x2, y2):
        self.action = action
        self.x1 = int(x1)
        self.y1 = int(y1)
        self.x2 = int(x2)
        self.y2 = int(y2)

    def has_point(self, x, y):
        return x >= self.x1 and x <= self.x2 and y >= self.y1 and y <= self.y2

class RaeMote(hildondesktop.HomePluginItem):
    DEBUG_RECTANGLES = False
    BASEDIR = '/opt/raemote/'
    REMOTE = 'apple'

    def __init__(self):
        hildondesktop.HomePluginItem.__init__(self)

        self.set_settings(True)
        self.connect('show-settings', self.on_settings)

        name_file = os.path.join(self.BASEDIR, self.REMOTE, 'name')
        rect_file = os.path.join(self.BASEDIR, self.REMOTE, 'rects')
        image_file = os.path.join(self.BASEDIR, self.REMOTE, 'bg.png')

        self.remote_name = open(name_file).read().strip()
        self.rectangles = self.load_rectangles(rect_file)
        self.image = cairo.ImageSurface.create_from_png(image_file)

        screen = self.get_screen()
        colormap = screen.get_rgba_colormap()
        self.set_colormap(colormap)

        self.overlay = None
        self._clear_overlay_source_id = None
        self.set_size_request(self.image.get_width(),
                              self.image.get_height())

        self.connect('expose-event', self.on_expose_event)
        self.connect('button-press-event', self.on_button_press)
        self.connect('button-release-event', self.on_button_release)
        self.set_events(gtk.gdk.BUTTON_PRESS_MASK | \
                        gtk.gdk.BUTTON_RELEASE_MASK)

        # Make sure lircd is running
        restart_lirc()

    def load_rectangles(self, filename):
        result = []
        for rect_def in open(filename).read().strip().splitlines():
            if rect_def and not rect_def.startswith('#'):
                result.append(Rectangle(*(rect_def.split())))
        return result

    def load_overlay(self, action):
        overlay_file = os.path.join(self.BASEDIR, self.REMOTE, action+'.png')
        return cairo.ImageSurface.create_from_png(overlay_file)

    def on_settings(self, widget):
        dialog = gtk.Dialog()
        dialog.set_title('About RaeMote')
        dialog.add_button('Visit website', 1)
        dialog.add_button('Report bug', 2)
        dialog.add_button('Donate', 3)
        dialog.vbox.add(gtk.Label('Version 1.2 (2010-03-20)'))
        dialog.vbox.add(gtk.Label('Copyright (c) 2010 Thomas Perl <http://thpinfo.com>'))
        dialog.show_all()
        result = dialog.run()
        dialog.destroy()
        if result >= 1 and result <= 3:
            ctx = osso.Context('com.thpinfo.raemote', '1.0', False)
            rpc = osso.Rpc(ctx)
            if result == 1:
                rpc.rpc_run_with_defaults('osso_browser', 'open_new_window',
                        ('http://thpinfo.com/2010/raemote',))
            elif result == 2:
                rpc.rpc_run_with_defaults('osso_browser', 'open_new_window',
                        ('http://talk.maemo.org/showthread.php?t=46870',))
            elif result == 3:
                rpc.rpc_run_with_defaults('osso_browser', 'open_new_window',
                        ('http://thpinfo.com/2010/raemote/donate',))

    def on_button_press(self, widget, event):
        if self.overlay is None:
            for rectangle in self.rectangles:
                if rectangle.has_point(event.x, event.y):
                    self.overlay = self.load_overlay(rectangle.action)
                    self.queue_draw()
                    send_ir(self.remote_name, rectangle.action)

                    # Remove the overlay after 100ms
                    self._clear_overlay_source_id = \
                            gobject.timeout_add(100, self.on_button_release)
                    return

    def on_button_release(self, widget=None, event=None):
        if self._clear_overlay_source_id is not None:
            gobject.source_remove(self._clear_overlay_source_id)
            self.overlay = None
            self.queue_draw()
            self._clear_overlay_source_id = None

        return False

    def on_expose_event(self, widget, event):
        cr = widget.window.cairo_create()
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.rectangle(0, 0, self.image.get_width(),
                           self.image.get_height())
        cr.set_source_rgba(0, 0, 0, 0)
        cr.fill()

        cr.rectangle(0, 0, self.image.get_width(),
                           self.image.get_height())
        cr.set_source_surface(self.image)
        cr.fill()

        cr.set_operator(cairo.OPERATOR_OVER)
        if self.DEBUG_RECTANGLES:
            for rectangle in self.rectangles:
                cr.set_source_rgba(1, 0, 1, .5)
                cr.rectangle(rectangle.x1, rectangle.y1,
                             rectangle.x2-rectangle.x1,
                             rectangle.y2-rectangle.y1)
                cr.fill()

        if self.overlay is not None:
            cr.set_operator(cairo.OPERATOR_OVER)
            cr.rectangle(0, 0, self.overlay.get_width(),
                               self.overlay.get_height())
            cr.set_source_surface(self.overlay)
            cr.fill()

        return True

hd_plugin_type = RaeMote

if __name__ == '__main__':
    gobject.type_register(hd_plugin_type)
    obj = gobject.new(hd_plugin_type, \
            plugin_id=hd_plugin_type.__class__.__name__)
    obj.show_all()
    gtk.main()

